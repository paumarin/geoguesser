package cat.itb.geoguesser;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.PersistableBundle;
import android.widget.*;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;

import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    ImageView title;
    TextView questionText;
    TextView number;
    ProgressBar progressBar;
    Button option1;
    Button option2;
    Button option3;
    Button option4;
    Button hint;
    int index;
    int i;
    int nHint;
    float resultado;
    AlertDialog.Builder alertDialog;
    Random rand = new Random();
    Question[] questions;
    ArrayList<Integer> numberUsed = new ArrayList<>();
    Answer[] answers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        title = findViewById(R.id.title);
        questionText = findViewById(R.id.questionText);
        number = findViewById(R.id.number);
        progressBar = findViewById(R.id.progressBar);
        option1 = findViewById(R.id.option1);
        option2 = findViewById(R.id.option2);
        option3 = findViewById(R.id.option3);
        option4 = findViewById(R.id.option4);
        hint = findViewById(R.id.hint);
        nHint = 3;
        resultado = 1;


        questions = new Question[]{
                new Question(R.string.q1, 1, R.drawable.fla),
                new Question(R.string.q2, 3, R.drawable.mapspainn),
                new Question(R.string.q3, 4, R.drawable.cerveza),
                new Question(R.string.q4, 2, R.drawable.madri),
                new Question(R.string.q5, 1, R.drawable.mapspainn),
                new Question(R.string.q6, 3, R.drawable.strait),
                new Question(R.string.q7, 2, R.drawable.dictatto),
                new Question(R.string.q8, 4, R.drawable.mapspainn),
                new Question(R.string.q9, 1, R.drawable.sea),
                new Question(R.string.q10, 1, R.drawable.artist),
        };
        answers = new Answer[]{
                new Answer(R.string.q1a1, R.string.q1a2, R.string.q1a3, R.string.q1a4, questions[0].getAnswer()),
                new Answer(R.string.q2a1, R.string.q2a2, R.string.q2a3, R.string.q2a4, questions[1].getAnswer()),
                new Answer(R.string.q3a1, R.string.q3a2, R.string.q3a3, R.string.q3a4, questions[2].getAnswer()),
                new Answer(R.string.q4a1, R.string.q4a2, R.string.q4a3, R.string.q4a4, questions[3].getAnswer()),
                new Answer(R.string.q5a1, R.string.q5a2, R.string.q5a3, R.string.q5a4, questions[4].getAnswer()),
                new Answer(R.string.q6a1, R.string.q6a2, R.string.q6a3, R.string.q6a4, questions[5].getAnswer()),
                new Answer(R.string.q7a1, R.string.q7a2, R.string.q7a3, R.string.q7a4, questions[6].getAnswer()),
                new Answer(R.string.q8a1, R.string.q8a2, R.string.q8a3, R.string.q8a4, questions[7].getAnswer()),
                new Answer(R.string.q9a1, R.string.q9a2, R.string.q9a3, R.string.q9a4, questions[8].getAnswer()),
                new Answer(R.string.q10a1, R.string.q10a2, R.string.q10a3, R.string.q10a4, questions[9].getAnswer())
        };


        i = 1;
        index = rand.nextInt(10);
        numberUsed.add(index);
        questionText.setText(questions[index].getText());
        title.setImageResource(questions[index].getImage());
        buttonAnswer(answers);
        progressBar.setMax(10);
        progressBar.setProgress(i);
        number.setText((i) + " of 10");


        alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("You finished the quiz!!");
        alertDialog.setMessage("What do you want to do next?");
        alertDialog.setNegativeButton("Reiniciar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                i = 1;
                progressBar.setProgress(i);
                nHint = 3;
                resultado = 1;
                numberUsed.clear();
                index = rand.nextInt(10);
                numberUsed.add(index);
                questionText.setText(questions[index].getText());
                title.setImageResource(questions[index].getImage());
                buttonAnswer(answers);
                progressBar.setMax(10);
                progressBar.setProgress(i);
                number.setText((i) + " of 10");
                hint.setText("Hint : " + nHint);
                hint.setVisibility(View.VISIBLE);

            }
        });
        alertDialog.setPositiveButton("Cerrar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });


        hint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (nHint <= 0) {

                } else {
                    hint.setText(String.valueOf(questions[index].getAnswer()));
                    nHint--;
                }

            }
        });

        option1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (i != 10) {
                    options(questions[index], 1);
                    index = randomQuestion(numberUsed);
                    i++;
                    questionText.setText(questions[index].getText());
                    progressBar.setProgress(i);
                    buttonAnswer(answers);
                    title.setImageResource(questions[index].getImage());
                    number.setText((i) + " of 10");
                    if (nHint <= 0) {
                        hint.setVisibility(View.INVISIBLE);
                    } else {
                        hint.setText("Hint : " + nHint);
                    }

                } else {
                    options(questions[index], 1);
                    AlertDialog dialog = alertDialog.create();
                    dialog.setMessage("You finished the quiz with a puntuation : " + (resultado * 10) + " of 100");
                    dialog.show();
                }

            }
        });
        option2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (i != 10) {
                    options(questions[index], 2);
                    index = randomQuestion(numberUsed);
                    i++;
                    questionText.setText(questions[index].getText());
                    progressBar.setProgress(i);
                    buttonAnswer(answers);
                    title.setImageResource(questions[index].getImage());
                    number.setText((i) + " of 10");
                    if (nHint <= 0) {
                        hint.setVisibility(View.INVISIBLE);
                    } else {
                        hint.setText("Hint : " + nHint);
                    }
                } else {
                    options(questions[index], 2);
                    AlertDialog dialog = alertDialog.create();
                    dialog.setMessage("You finished the quiz with a puntuation : " + (resultado * 10) + " of 100");
                    dialog.show();
                }

            }
        });
        option3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (i != 10) {
                    options(questions[index], 3);
                    index = randomQuestion(numberUsed);
                    i++;
                    questionText.setText(questions[index].getText());
                    progressBar.setProgress(i);
                    buttonAnswer(answers);
                    title.setImageResource(questions[index].getImage());
                    number.setText((i) + " of 10");
                    if (nHint <= 0) {
                        hint.setVisibility(View.INVISIBLE);
                    } else {
                        hint.setText("Hint : " + nHint);
                    }
                } else {
                    options(questions[index], 3);
                    AlertDialog dialog = alertDialog.create();
                    dialog.setMessage("You finished the quiz with a puntuation : " + (resultado * 10) + " of 100");
                    dialog.show();
                }

            }
        });
        option4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (i != 10) {
                    options(questions[index], 4);
                    index = randomQuestion(numberUsed);
                    i++;
                    questionText.setText(questions[index].getText());
                    progressBar.setProgress(i);
                    buttonAnswer(answers);
                    title.setImageResource(questions[index].getImage());
                    number.setText((i) + " of 10");
                    if (nHint <= 0) {
                        hint.setVisibility(View.INVISIBLE);
                    } else {
                        hint.setText("Hint : " + nHint);
                    }
                } else {
                    options(questions[index], 4);
                    AlertDialog dialog = alertDialog.create();
                    dialog.setMessage("You finished the quiz with a puntuation : " + (resultado * 10) + " of 100");
                    dialog.show();
                }
            }
        });


    }

    public void options(Question question, int number) {
        if (question.answer == number) {
            Toast.makeText(MainActivity.this, "Respuesta correcta", Toast.LENGTH_SHORT).show();
            resultado += 1;
        } else {
            Toast.makeText(MainActivity.this, "Has fallado", Toast.LENGTH_SHORT).show();
            resultado -= 0.5;
        }
    }

    public int randomQuestion(ArrayList usedNumbers) {
        boolean nuevo = false;

        while (usedNumbers.contains(index) && i != 10) {
            index = rand.nextInt(10);
        }
        usedNumbers.add(index);

        return index;
    }

    public void buttonAnswer(Answer[] answers) {
        option1.setText(answers[index].getPrimeraR());
        option2.setText(answers[index].getSegundaR());
        option3.setText(answers[index].getTerceraR());
        option4.setText(answers[index].getCuartaR());
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putInt("i", i);
        savedInstanceState.putInt("index", index);
        savedInstanceState.putFloat("resultado", resultado);
        savedInstanceState.putIntegerArrayList("numberUsed", numberUsed);
        savedInstanceState.putInt("nHint", nHint);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        i = savedInstanceState.getInt("i");
        index = savedInstanceState.getInt("index");
        resultado = savedInstanceState.getFloat("resultado");
        numberUsed = savedInstanceState.getIntegerArrayList("numberUsed");
        progressBar.setProgress(i);
        nHint = savedInstanceState.getInt("nHint");
        questionText.setText(questions[index].getText());
        title.setImageResource(questions[index].getImage());
        buttonAnswer(answers);
        progressBar.setMax(10);
        progressBar.setProgress(i);
        number.setText((i) + " of 10");
        hint.setText("Hint : " + nHint);
    }
}