package cat.itb.geoguesser;

public class Answer {
    int primeraR;
    int segundaR;
    int terceraR;
    int cuartaR;
    int question;

    public Answer(int primeraR , int segundaR, int terceraR, int cuartaR, int question){
        this.primeraR = primeraR;
        this.segundaR = segundaR;
        this.terceraR = terceraR;
        this.cuartaR = cuartaR;
        this.question = question;
    }


    public int getPrimeraR() {
        return primeraR;
    }

    public void setPrimeraR(int primeraR) {
        this.primeraR = primeraR;
    }

    public int getSegundaR() {
        return segundaR;
    }

    public void setSegundaR(int segundaR) {
        this.segundaR = segundaR;
    }

    public int getTerceraR() {
        return terceraR;
    }

    public void setTerceraR(int terceraR) {
        this.terceraR = terceraR;
    }

    public int getCuartaR() {
        return cuartaR;
    }

    public void setCuartaR(int cuartaR) {
        this.cuartaR = cuartaR;
    }

    public int getQuestion() {
        return question;
    }

    public void setQuestion(int question) {
        this.question = question;
    }
}
