package cat.itb.geoguesser;

public class Question {

    int text;

    int image;

    int answer;

    public int getImage() {
        return image;
    }

    public Question(int text , int answer, int image){
        this.text = text;
        this.answer = answer;
        this.image = image;
    }

    public int getAnswer() {
        return answer;
    }
    public int getText() {
        return text;
    }

    public void setText(int text) {
        this.text = text;
    }

    public int isAnswer() {
        return answer;
    }

    public void setAnswer(int answer) {
        this.answer = answer;
    }




}
